// ==UserScript==
// @name         KIF 49,0 dark mode
// @namespace    https://event.kif.rocks/
// @version      0.4.2
// @description  Make it dark, make it better!
// @author       xoria
// @match        https://event.kif.rocks/**
// @match        https://*.meet.gwdg.de/**
// @match        https://ak.kif.rocks/kif490/**
// @match        https://wiki.kif.rocks/wiki/KIF490:FAQ
// @match        https://wiki.kif.rocks/wiki/KIF490:CoC
// @match        https://wiki.kif.rocks/wiki/KIF490:Supporters
// @grant        none
// @run-at document-end
// ==/UserScript==

(function() {
    'use strict';

    const styleEl = document.createElement("style");
    document.head.append(styleEl);
    console.log(window.document);

    styleEl.innerHTML = `
#app, #app :root, :root {
  --clr-primary: #4e9ade !important;
  --clr-sidebar: #330515 !important;
  --clr-bbb-background: #00204a;
  --clr-primary-darken-15: #6fb3fb !important;
  --clr-primary-darken-20: #8dc4ff !important;
  --clr-primary-alpha-60: rgba(78, 154, 222, 0.6) !important;
  --clr-primary-alpha-50: rgba(78, 154, 222, 0.5) !important;
  --clr-primary-alpha-18: rgba(78, 154, 222, 0.18) !important;
  --clr-text-primary: rgba(176, 176, 176, 0.87) !important;
  --clr-text-secondary: rgba(176, 176, 176, 0.54) !important;
  --clr-text-disabled: rgba(176, 176, 176, 0.38) !important;
  --clr-dividers: rgba(23, 23, 23, 0.63) !important;
  --clr-input-primary-bg: #4e9ade !important;
  --clr-input-primary-fg: #171717 !important;
  --clr-input-primary-bg-darken: var(--clr-primary-darken-20) !important;
  --clr-input-secondary-fg: #4e9ade !important;
  --clr-input-secondary-fg-alpha: rgba(78, 154, 222, 0.08) !important;
  --clr-sidebar-text-primary: #d2d2d2 !important;
  --clr-sidebar-text-secondary: rgba(210, 210, 210, 0.7) !important;
  --clr-sidebar-text-disabled: rgba(210, 210, 210, 0.5) !important;
  --clr-sidebar-active-bg: rgba(210, 210, 210, 0.18) !important;
  --clr-sidebar-active-fg: #d2d2d2 !important;
  --clr-sidebar-hover-bg: rgba(210, 210, 210, 0.24) !important;
  --clr-sidebar-hover-fg: #d2d2d2 !important;
  --main-bg: #171717;
  --main-fg: #b0b0b0;
  --link-hover: #c6e0ff;
  --scrollbar-rail: rgba(79, 78, 78, 0.5);
  --user-list-bg: var(--main-bg);
  --user-list-text: var(--main-fg);
  --color-off-white: var(--clr-sidebar-text-primary);
  --color-text: var(--main-fg);
  --color-heading: var(--clr-sidebar-text-primary);
  --userlist-handle-width: 2px;
}
`;

    if (document.location.host === "event.kif.rocks") {
      styleEl.innerHTML += `
#app {
  font-weight: 500;
  font-family: Montserrat, Roboto, Helvetica Neue, HelveticaNeue, Helvetica, Arial, Microsoft Yahei, 微软雅黑, STXihei, 华文细黑, sans-serif;
  letter-spacing: 0.02em;
}
#app .scrollbar-rail-y, #app .bunt-scrollbar .bunt-scrollbar-rail-wrapper-y .bunt-scrollbar-rail-y {
  background-color: var(--scrollbar-rail);
  width: 10px;
}
#app .c-scrollbars .scrollbar-rail-y .scrollbar-thumb, #app .bunt-scrollbar .bunt-scrollbar-rail-wrapper-y .bunt-scrollbar-rail-y .bunt-scrollbar-thumb {
  width: 10px;
  opacity: 0.4;
  border-radius: 0;
  right: 0;
}
#app .bunt-tooltip {
  background-color: var(--main-bg);
}
#app .c-rooms-sidebar > .profile .c-avatar {
  background-color: var(--clr-input-primary-fg);
}
#app .c-room, #app .c-userlist, #app .c-userlist .profile .content .exhibitors .exhibitor, #app .c-userlist .profile .content .exhibitors .exhibitor .short-text, #app .c-userlist .user-list, #app .c-exhibition, #app .c-exhibition .exhibitor, #app .c-exhibition .exhibitor .short-text, #app .v-preferences, #app .c-channel, #app .c-chat, #app .actions, #app .emoji-mart, #app .emoji-mart-category-label span, #app .background-room, #app .c-prompt .prompt-wrapper, #app .c-chat-user-card .user-card, #app .c-contact-requests, #app .c-contact-requests .header, #app .c-exhibitors, #app .c-exhibitors .exhibitor-list .header, #app .c-exhibitors .exhibitor-list .name {
  background-color: var(--main-bg);
  color: var(--main-fg);
}
#app .c-room a, #app .c-userlist a, #app .c-userlist .profile .content .exhibitors .exhibitor a, #app .c-userlist .profile .content .exhibitors .exhibitor .short-text a, #app .c-userlist .user-list a, #app .c-exhibition a, #app .c-exhibition .exhibitor a, #app .c-exhibition .exhibitor .short-text a, #app .v-preferences a, #app .c-channel a, #app .c-chat a, #app .actions a, #app .emoji-mart a, #app .emoji-mart-category-label span a, #app .background-room a, #app .c-prompt .prompt-wrapper a, #app .c-chat-user-card .user-card a, #app .c-contact-requests a, #app .c-contact-requests .header a, #app .c-exhibitors a, #app .c-exhibitors .exhibitor-list .header a, #app .c-exhibitors .exhibitor-list .name a, #app .c-room .profile .actions .action-row .bunt-button, #app .c-userlist .profile .actions .action-row .bunt-button, #app .c-userlist .profile .content .exhibitors .exhibitor .profile .actions .action-row .bunt-button, #app .c-userlist .profile .content .exhibitors .exhibitor .short-text .profile .actions .action-row .bunt-button, #app .c-userlist .user-list .profile .actions .action-row .bunt-button, #app .c-exhibition .profile .actions .action-row .bunt-button, #app .c-exhibition .exhibitor .profile .actions .action-row .bunt-button, #app .c-exhibition .exhibitor .short-text .profile .actions .action-row .bunt-button, #app .v-preferences .profile .actions .action-row .bunt-button, #app .c-channel .profile .actions .action-row .bunt-button, #app .c-chat .profile .actions .action-row .bunt-button, #app .actions .profile .actions .action-row .bunt-button, #app .emoji-mart .profile .actions .action-row .bunt-button, #app .emoji-mart-category-label span .profile .actions .action-row .bunt-button, #app .background-room .profile .actions .action-row .bunt-button, #app .c-prompt .prompt-wrapper .profile .actions .action-row .bunt-button, #app .c-chat-user-card .user-card .profile .actions .action-row .bunt-button, #app .c-contact-requests .profile .actions .action-row .bunt-button, #app .c-contact-requests .header .profile .actions .action-row .bunt-button, #app .c-exhibitors .profile .actions .action-row .bunt-button, #app .c-exhibitors .exhibitor-list .header .profile .actions .action-row .bunt-button, #app .c-exhibitors .exhibitor-list .name .profile .actions .action-row .bunt-button, #app .c-room .bunt-icon-button .bunt-icon, #app .c-userlist .bunt-icon-button .bunt-icon, #app .c-userlist .profile .content .exhibitors .exhibitor .bunt-icon-button .bunt-icon, #app .c-userlist .profile .content .exhibitors .exhibitor .short-text .bunt-icon-button .bunt-icon, #app .c-userlist .user-list .bunt-icon-button .bunt-icon, #app .c-exhibition .bunt-icon-button .bunt-icon, #app .c-exhibition .exhibitor .bunt-icon-button .bunt-icon, #app .c-exhibition .exhibitor .short-text .bunt-icon-button .bunt-icon, #app .v-preferences .bunt-icon-button .bunt-icon, #app .c-channel .bunt-icon-button .bunt-icon, #app .c-chat .bunt-icon-button .bunt-icon, #app .actions .bunt-icon-button .bunt-icon, #app .emoji-mart .bunt-icon-button .bunt-icon, #app .emoji-mart-category-label span .bunt-icon-button .bunt-icon, #app .background-room .bunt-icon-button .bunt-icon, #app .c-prompt .prompt-wrapper .bunt-icon-button .bunt-icon, #app .c-chat-user-card .user-card .bunt-icon-button .bunt-icon, #app .c-contact-requests .bunt-icon-button .bunt-icon, #app .c-contact-requests .header .bunt-icon-button .bunt-icon, #app .c-exhibitors .bunt-icon-button .bunt-icon, #app .c-exhibitors .exhibitor-list .header .bunt-icon-button .bunt-icon, #app .c-exhibitors .exhibitor-list .name .bunt-icon-button .bunt-icon, #app .c-room .c-emoji-picker-button .btn-emoji-picker svg path, #app .c-userlist .c-emoji-picker-button .btn-emoji-picker svg path, #app .c-userlist .profile .content .exhibitors .exhibitor .c-emoji-picker-button .btn-emoji-picker svg path, #app .c-userlist .profile .content .exhibitors .exhibitor .short-text .c-emoji-picker-button .btn-emoji-picker svg path, #app .c-userlist .user-list .c-emoji-picker-button .btn-emoji-picker svg path, #app .c-exhibition .c-emoji-picker-button .btn-emoji-picker svg path, #app .c-exhibition .exhibitor .c-emoji-picker-button .btn-emoji-picker svg path, #app .c-exhibition .exhibitor .short-text .c-emoji-picker-button .btn-emoji-picker svg path, #app .v-preferences .c-emoji-picker-button .btn-emoji-picker svg path, #app .c-channel .c-emoji-picker-button .btn-emoji-picker svg path, #app .c-chat .c-emoji-picker-button .btn-emoji-picker svg path, #app .actions .c-emoji-picker-button .btn-emoji-picker svg path, #app .emoji-mart .c-emoji-picker-button .btn-emoji-picker svg path, #app .emoji-mart-category-label span .c-emoji-picker-button .btn-emoji-picker svg path, #app .background-room .c-emoji-picker-button .btn-emoji-picker svg path, #app .c-prompt .prompt-wrapper .c-emoji-picker-button .btn-emoji-picker svg path, #app .c-chat-user-card .user-card .c-emoji-picker-button .btn-emoji-picker svg path, #app .c-contact-requests .c-emoji-picker-button .btn-emoji-picker svg path, #app .c-contact-requests .header .c-emoji-picker-button .btn-emoji-picker svg path, #app .c-exhibitors .c-emoji-picker-button .btn-emoji-picker svg path, #app .c-exhibitors .exhibitor-list .header .c-emoji-picker-button .btn-emoji-picker svg path, #app .c-exhibitors .exhibitor-list .name .c-emoji-picker-button .btn-emoji-picker svg path, #app .c-room .c-chat-user-card .user-card .actions .bunt-button, #app .c-userlist .c-chat-user-card .user-card .actions .bunt-button, #app .c-userlist .profile .content .exhibitors .exhibitor .c-chat-user-card .user-card .actions .bunt-button, #app .c-userlist .profile .content .exhibitors .exhibitor .short-text .c-chat-user-card .user-card .actions .bunt-button, #app .c-userlist .user-list .c-chat-user-card .user-card .actions .bunt-button, #app .c-exhibition .c-chat-user-card .user-card .actions .bunt-button, #app .c-exhibition .exhibitor .c-chat-user-card .user-card .actions .bunt-button, #app .c-exhibition .exhibitor .short-text .c-chat-user-card .user-card .actions .bunt-button, #app .v-preferences .c-chat-user-card .user-card .actions .bunt-button, #app .c-channel .c-chat-user-card .user-card .actions .bunt-button, #app .c-chat .c-chat-user-card .user-card .actions .bunt-button, #app .actions .c-chat-user-card .user-card .actions .bunt-button, #app .emoji-mart .c-chat-user-card .user-card .actions .bunt-button, #app .emoji-mart-category-label span .c-chat-user-card .user-card .actions .bunt-button, #app .background-room .c-chat-user-card .user-card .actions .bunt-button, #app .c-prompt .prompt-wrapper .c-chat-user-card .user-card .actions .bunt-button, #app .c-chat-user-card .user-card .c-chat-user-card .user-card .actions .bunt-button, #app .c-contact-requests .c-chat-user-card .user-card .actions .bunt-button, #app .c-contact-requests .header .c-chat-user-card .user-card .actions .bunt-button, #app .c-exhibitors .c-chat-user-card .user-card .actions .bunt-button, #app .c-exhibitors .exhibitor-list .header .c-chat-user-card .user-card .actions .bunt-button, #app .c-exhibitors .exhibitor-list .name .c-chat-user-card .user-card .actions .bunt-button, #app .c-room .contact-requests-list .actions .btn-open-dm, #app .c-userlist .contact-requests-list .actions .btn-open-dm, #app .c-userlist .profile .content .exhibitors .exhibitor .contact-requests-list .actions .btn-open-dm, #app .c-userlist .profile .content .exhibitors .exhibitor .short-text .contact-requests-list .actions .btn-open-dm, #app .c-userlist .user-list .contact-requests-list .actions .btn-open-dm, #app .c-exhibition .contact-requests-list .actions .btn-open-dm, #app .c-exhibition .exhibitor .contact-requests-list .actions .btn-open-dm, #app .c-exhibition .exhibitor .short-text .contact-requests-list .actions .btn-open-dm, #app .v-preferences .contact-requests-list .actions .btn-open-dm, #app .c-channel .contact-requests-list .actions .btn-open-dm, #app .c-chat .contact-requests-list .actions .btn-open-dm, #app .actions .contact-requests-list .actions .btn-open-dm, #app .emoji-mart .contact-requests-list .actions .btn-open-dm, #app .emoji-mart-category-label span .contact-requests-list .actions .btn-open-dm, #app .background-room .contact-requests-list .actions .btn-open-dm, #app .c-prompt .prompt-wrapper .contact-requests-list .actions .btn-open-dm, #app .c-chat-user-card .user-card .contact-requests-list .actions .btn-open-dm, #app .c-contact-requests .contact-requests-list .actions .btn-open-dm, #app .c-contact-requests .header .contact-requests-list .actions .btn-open-dm, #app .c-exhibitors .contact-requests-list .actions .btn-open-dm, #app .c-exhibitors .exhibitor-list .header .contact-requests-list .actions .btn-open-dm, #app .c-exhibitors .exhibitor-list .name .contact-requests-list .actions .btn-open-dm {
  color: var(--clr-primary) !important;
  font-weight: 600;
  fill: currentColor !important;
}
#app .c-room a:hover, #app .c-userlist a:hover, #app .c-userlist .profile .content .exhibitors .exhibitor a:hover, #app .c-userlist .profile .content .exhibitors .exhibitor .short-text a:hover, #app .c-userlist .user-list a:hover, #app .c-exhibition a:hover, #app .c-exhibition .exhibitor a:hover, #app .c-exhibition .exhibitor .short-text a:hover, #app .v-preferences a:hover, #app .c-channel a:hover, #app .c-chat a:hover, #app .actions a:hover, #app .emoji-mart a:hover, #app .emoji-mart-category-label span a:hover, #app .background-room a:hover, #app .c-prompt .prompt-wrapper a:hover, #app .c-chat-user-card .user-card a:hover, #app .c-contact-requests a:hover, #app .c-contact-requests .header a:hover, #app .c-exhibitors a:hover, #app .c-exhibitors .exhibitor-list .header a:hover, #app .c-exhibitors .exhibitor-list .name a:hover, #app .c-room .profile .actions .action-row .bunt-button:hover, #app .c-userlist .profile .actions .action-row .bunt-button:hover, #app .c-userlist .profile .content .exhibitors .exhibitor .profile .actions .action-row .bunt-button:hover, #app .c-userlist .profile .content .exhibitors .exhibitor .short-text .profile .actions .action-row .bunt-button:hover, #app .c-userlist .user-list .profile .actions .action-row .bunt-button:hover, #app .c-exhibition .profile .actions .action-row .bunt-button:hover, #app .c-exhibition .exhibitor .profile .actions .action-row .bunt-button:hover, #app .c-exhibition .exhibitor .short-text .profile .actions .action-row .bunt-button:hover, #app .v-preferences .profile .actions .action-row .bunt-button:hover, #app .c-channel .profile .actions .action-row .bunt-button:hover, #app .c-chat .profile .actions .action-row .bunt-button:hover, #app .actions .profile .actions .action-row .bunt-button:hover, #app .emoji-mart .profile .actions .action-row .bunt-button:hover, #app .emoji-mart-category-label span .profile .actions .action-row .bunt-button:hover, #app .background-room .profile .actions .action-row .bunt-button:hover, #app .c-prompt .prompt-wrapper .profile .actions .action-row .bunt-button:hover, #app .c-chat-user-card .user-card .profile .actions .action-row .bunt-button:hover, #app .c-contact-requests .profile .actions .action-row .bunt-button:hover, #app .c-contact-requests .header .profile .actions .action-row .bunt-button:hover, #app .c-exhibitors .profile .actions .action-row .bunt-button:hover, #app .c-exhibitors .exhibitor-list .header .profile .actions .action-row .bunt-button:hover, #app .c-exhibitors .exhibitor-list .name .profile .actions .action-row .bunt-button:hover, #app .c-room .bunt-icon-button .bunt-icon:hover, #app .c-userlist .bunt-icon-button .bunt-icon:hover, #app .c-userlist .profile .content .exhibitors .exhibitor .bunt-icon-button .bunt-icon:hover, #app .c-userlist .profile .content .exhibitors .exhibitor .short-text .bunt-icon-button .bunt-icon:hover, #app .c-userlist .user-list .bunt-icon-button .bunt-icon:hover, #app .c-exhibition .bunt-icon-button .bunt-icon:hover, #app .c-exhibition .exhibitor .bunt-icon-button .bunt-icon:hover, #app .c-exhibition .exhibitor .short-text .bunt-icon-button .bunt-icon:hover, #app .v-preferences .bunt-icon-button .bunt-icon:hover, #app .c-channel .bunt-icon-button .bunt-icon:hover, #app .c-chat .bunt-icon-button .bunt-icon:hover, #app .actions .bunt-icon-button .bunt-icon:hover, #app .emoji-mart .bunt-icon-button .bunt-icon:hover, #app .emoji-mart-category-label span .bunt-icon-button .bunt-icon:hover, #app .background-room .bunt-icon-button .bunt-icon:hover, #app .c-prompt .prompt-wrapper .bunt-icon-button .bunt-icon:hover, #app .c-chat-user-card .user-card .bunt-icon-button .bunt-icon:hover, #app .c-contact-requests .bunt-icon-button .bunt-icon:hover, #app .c-contact-requests .header .bunt-icon-button .bunt-icon:hover, #app .c-exhibitors .bunt-icon-button .bunt-icon:hover, #app .c-exhibitors .exhibitor-list .header .bunt-icon-button .bunt-icon:hover, #app .c-exhibitors .exhibitor-list .name .bunt-icon-button .bunt-icon:hover, #app .c-room .c-emoji-picker-button .btn-emoji-picker svg path:hover, #app .c-userlist .c-emoji-picker-button .btn-emoji-picker svg path:hover, #app .c-userlist .profile .content .exhibitors .exhibitor .c-emoji-picker-button .btn-emoji-picker svg path:hover, #app .c-userlist .profile .content .exhibitors .exhibitor .short-text .c-emoji-picker-button .btn-emoji-picker svg path:hover, #app .c-userlist .user-list .c-emoji-picker-button .btn-emoji-picker svg path:hover, #app .c-exhibition .c-emoji-picker-button .btn-emoji-picker svg path:hover, #app .c-exhibition .exhibitor .c-emoji-picker-button .btn-emoji-picker svg path:hover, #app .c-exhibition .exhibitor .short-text .c-emoji-picker-button .btn-emoji-picker svg path:hover, #app .v-preferences .c-emoji-picker-button .btn-emoji-picker svg path:hover, #app .c-channel .c-emoji-picker-button .btn-emoji-picker svg path:hover, #app .c-chat .c-emoji-picker-button .btn-emoji-picker svg path:hover, #app .actions .c-emoji-picker-button .btn-emoji-picker svg path:hover, #app .emoji-mart .c-emoji-picker-button .btn-emoji-picker svg path:hover, #app .emoji-mart-category-label span .c-emoji-picker-button .btn-emoji-picker svg path:hover, #app .background-room .c-emoji-picker-button .btn-emoji-picker svg path:hover, #app .c-prompt .prompt-wrapper .c-emoji-picker-button .btn-emoji-picker svg path:hover, #app .c-chat-user-card .user-card .c-emoji-picker-button .btn-emoji-picker svg path:hover, #app .c-contact-requests .c-emoji-picker-button .btn-emoji-picker svg path:hover, #app .c-contact-requests .header .c-emoji-picker-button .btn-emoji-picker svg path:hover, #app .c-exhibitors .c-emoji-picker-button .btn-emoji-picker svg path:hover, #app .c-exhibitors .exhibitor-list .header .c-emoji-picker-button .btn-emoji-picker svg path:hover, #app .c-exhibitors .exhibitor-list .name .c-emoji-picker-button .btn-emoji-picker svg path:hover, #app .c-room .c-chat-user-card .user-card .actions .bunt-button:hover, #app .c-userlist .c-chat-user-card .user-card .actions .bunt-button:hover, #app .c-userlist .profile .content .exhibitors .exhibitor .c-chat-user-card .user-card .actions .bunt-button:hover, #app .c-userlist .profile .content .exhibitors .exhibitor .short-text .c-chat-user-card .user-card .actions .bunt-button:hover, #app .c-userlist .user-list .c-chat-user-card .user-card .actions .bunt-button:hover, #app .c-exhibition .c-chat-user-card .user-card .actions .bunt-button:hover, #app .c-exhibition .exhibitor .c-chat-user-card .user-card .actions .bunt-button:hover, #app .c-exhibition .exhibitor .short-text .c-chat-user-card .user-card .actions .bunt-button:hover, #app .v-preferences .c-chat-user-card .user-card .actions .bunt-button:hover, #app .c-channel .c-chat-user-card .user-card .actions .bunt-button:hover, #app .c-chat .c-chat-user-card .user-card .actions .bunt-button:hover, #app .actions .c-chat-user-card .user-card .actions .bunt-button:hover, #app .emoji-mart .c-chat-user-card .user-card .actions .bunt-button:hover, #app .emoji-mart-category-label span .c-chat-user-card .user-card .actions .bunt-button:hover, #app .background-room .c-chat-user-card .user-card .actions .bunt-button:hover, #app .c-prompt .prompt-wrapper .c-chat-user-card .user-card .actions .bunt-button:hover, #app .c-chat-user-card .user-card .c-chat-user-card .user-card .actions .bunt-button:hover, #app .c-contact-requests .c-chat-user-card .user-card .actions .bunt-button:hover, #app .c-contact-requests .header .c-chat-user-card .user-card .actions .bunt-button:hover, #app .c-exhibitors .c-chat-user-card .user-card .actions .bunt-button:hover, #app .c-exhibitors .exhibitor-list .header .c-chat-user-card .user-card .actions .bunt-button:hover, #app .c-exhibitors .exhibitor-list .name .c-chat-user-card .user-card .actions .bunt-button:hover, #app .c-room .contact-requests-list .actions .btn-open-dm:hover, #app .c-userlist .contact-requests-list .actions .btn-open-dm:hover, #app .c-userlist .profile .content .exhibitors .exhibitor .contact-requests-list .actions .btn-open-dm:hover, #app .c-userlist .profile .content .exhibitors .exhibitor .short-text .contact-requests-list .actions .btn-open-dm:hover, #app .c-userlist .user-list .contact-requests-list .actions .btn-open-dm:hover, #app .c-exhibition .contact-requests-list .actions .btn-open-dm:hover, #app .c-exhibition .exhibitor .contact-requests-list .actions .btn-open-dm:hover, #app .c-exhibition .exhibitor .short-text .contact-requests-list .actions .btn-open-dm:hover, #app .v-preferences .contact-requests-list .actions .btn-open-dm:hover, #app .c-channel .contact-requests-list .actions .btn-open-dm:hover, #app .c-chat .contact-requests-list .actions .btn-open-dm:hover, #app .actions .contact-requests-list .actions .btn-open-dm:hover, #app .emoji-mart .contact-requests-list .actions .btn-open-dm:hover, #app .emoji-mart-category-label span .contact-requests-list .actions .btn-open-dm:hover, #app .background-room .contact-requests-list .actions .btn-open-dm:hover, #app .c-prompt .prompt-wrapper .contact-requests-list .actions .btn-open-dm:hover, #app .c-chat-user-card .user-card .contact-requests-list .actions .btn-open-dm:hover, #app .c-contact-requests .contact-requests-list .actions .btn-open-dm:hover, #app .c-contact-requests .header .contact-requests-list .actions .btn-open-dm:hover, #app .c-exhibitors .contact-requests-list .actions .btn-open-dm:hover, #app .c-exhibitors .exhibitor-list .header .contact-requests-list .actions .btn-open-dm:hover, #app .c-exhibitors .exhibitor-list .name .contact-requests-list .actions .btn-open-dm:hover, #app .c-room a:focus, #app .c-userlist a:focus, #app .c-userlist .profile .content .exhibitors .exhibitor a:focus, #app .c-userlist .profile .content .exhibitors .exhibitor .short-text a:focus, #app .c-userlist .user-list a:focus, #app .c-exhibition a:focus, #app .c-exhibition .exhibitor a:focus, #app .c-exhibition .exhibitor .short-text a:focus, #app .v-preferences a:focus, #app .c-channel a:focus, #app .c-chat a:focus, #app .actions a:focus, #app .emoji-mart a:focus, #app .emoji-mart-category-label span a:focus, #app .background-room a:focus, #app .c-prompt .prompt-wrapper a:focus, #app .c-chat-user-card .user-card a:focus, #app .c-contact-requests a:focus, #app .c-contact-requests .header a:focus, #app .c-exhibitors a:focus, #app .c-exhibitors .exhibitor-list .header a:focus, #app .c-exhibitors .exhibitor-list .name a:focus, #app .c-room .profile .actions .action-row .bunt-button:focus, #app .c-userlist .profile .actions .action-row .bunt-button:focus, #app .c-userlist .profile .content .exhibitors .exhibitor .profile .actions .action-row .bunt-button:focus, #app .c-userlist .profile .content .exhibitors .exhibitor .short-text .profile .actions .action-row .bunt-button:focus, #app .c-userlist .user-list .profile .actions .action-row .bunt-button:focus, #app .c-exhibition .profile .actions .action-row .bunt-button:focus, #app .c-exhibition .exhibitor .profile .actions .action-row .bunt-button:focus, #app .c-exhibition .exhibitor .short-text .profile .actions .action-row .bunt-button:focus, #app .v-preferences .profile .actions .action-row .bunt-button:focus, #app .c-channel .profile .actions .action-row .bunt-button:focus, #app .c-chat .profile .actions .action-row .bunt-button:focus, #app .actions .profile .actions .action-row .bunt-button:focus, #app .emoji-mart .profile .actions .action-row .bunt-button:focus, #app .emoji-mart-category-label span .profile .actions .action-row .bunt-button:focus, #app .background-room .profile .actions .action-row .bunt-button:focus, #app .c-prompt .prompt-wrapper .profile .actions .action-row .bunt-button:focus, #app .c-chat-user-card .user-card .profile .actions .action-row .bunt-button:focus, #app .c-contact-requests .profile .actions .action-row .bunt-button:focus, #app .c-contact-requests .header .profile .actions .action-row .bunt-button:focus, #app .c-exhibitors .profile .actions .action-row .bunt-button:focus, #app .c-exhibitors .exhibitor-list .header .profile .actions .action-row .bunt-button:focus, #app .c-exhibitors .exhibitor-list .name .profile .actions .action-row .bunt-button:focus, #app .c-room .bunt-icon-button .bunt-icon:focus, #app .c-userlist .bunt-icon-button .bunt-icon:focus, #app .c-userlist .profile .content .exhibitors .exhibitor .bunt-icon-button .bunt-icon:focus, #app .c-userlist .profile .content .exhibitors .exhibitor .short-text .bunt-icon-button .bunt-icon:focus, #app .c-userlist .user-list .bunt-icon-button .bunt-icon:focus, #app .c-exhibition .bunt-icon-button .bunt-icon:focus, #app .c-exhibition .exhibitor .bunt-icon-button .bunt-icon:focus, #app .c-exhibition .exhibitor .short-text .bunt-icon-button .bunt-icon:focus, #app .v-preferences .bunt-icon-button .bunt-icon:focus, #app .c-channel .bunt-icon-button .bunt-icon:focus, #app .c-chat .bunt-icon-button .bunt-icon:focus, #app .actions .bunt-icon-button .bunt-icon:focus, #app .emoji-mart .bunt-icon-button .bunt-icon:focus, #app .emoji-mart-category-label span .bunt-icon-button .bunt-icon:focus, #app .background-room .bunt-icon-button .bunt-icon:focus, #app .c-prompt .prompt-wrapper .bunt-icon-button .bunt-icon:focus, #app .c-chat-user-card .user-card .bunt-icon-button .bunt-icon:focus, #app .c-contact-requests .bunt-icon-button .bunt-icon:focus, #app .c-contact-requests .header .bunt-icon-button .bunt-icon:focus, #app .c-exhibitors .bunt-icon-button .bunt-icon:focus, #app .c-exhibitors .exhibitor-list .header .bunt-icon-button .bunt-icon:focus, #app .c-exhibitors .exhibitor-list .name .bunt-icon-button .bunt-icon:focus, #app .c-room .c-emoji-picker-button .btn-emoji-picker svg path:focus, #app .c-userlist .c-emoji-picker-button .btn-emoji-picker svg path:focus, #app .c-userlist .profile .content .exhibitors .exhibitor .c-emoji-picker-button .btn-emoji-picker svg path:focus, #app .c-userlist .profile .content .exhibitors .exhibitor .short-text .c-emoji-picker-button .btn-emoji-picker svg path:focus, #app .c-userlist .user-list .c-emoji-picker-button .btn-emoji-picker svg path:focus, #app .c-exhibition .c-emoji-picker-button .btn-emoji-picker svg path:focus, #app .c-exhibition .exhibitor .c-emoji-picker-button .btn-emoji-picker svg path:focus, #app .c-exhibition .exhibitor .short-text .c-emoji-picker-button .btn-emoji-picker svg path:focus, #app .v-preferences .c-emoji-picker-button .btn-emoji-picker svg path:focus, #app .c-channel .c-emoji-picker-button .btn-emoji-picker svg path:focus, #app .c-chat .c-emoji-picker-button .btn-emoji-picker svg path:focus, #app .actions .c-emoji-picker-button .btn-emoji-picker svg path:focus, #app .emoji-mart .c-emoji-picker-button .btn-emoji-picker svg path:focus, #app .emoji-mart-category-label span .c-emoji-picker-button .btn-emoji-picker svg path:focus, #app .background-room .c-emoji-picker-button .btn-emoji-picker svg path:focus, #app .c-prompt .prompt-wrapper .c-emoji-picker-button .btn-emoji-picker svg path:focus, #app .c-chat-user-card .user-card .c-emoji-picker-button .btn-emoji-picker svg path:focus, #app .c-contact-requests .c-emoji-picker-button .btn-emoji-picker svg path:focus, #app .c-contact-requests .header .c-emoji-picker-button .btn-emoji-picker svg path:focus, #app .c-exhibitors .c-emoji-picker-button .btn-emoji-picker svg path:focus, #app .c-exhibitors .exhibitor-list .header .c-emoji-picker-button .btn-emoji-picker svg path:focus, #app .c-exhibitors .exhibitor-list .name .c-emoji-picker-button .btn-emoji-picker svg path:focus, #app .c-room .c-chat-user-card .user-card .actions .bunt-button:focus, #app .c-userlist .c-chat-user-card .user-card .actions .bunt-button:focus, #app .c-userlist .profile .content .exhibitors .exhibitor .c-chat-user-card .user-card .actions .bunt-button:focus, #app .c-userlist .profile .content .exhibitors .exhibitor .short-text .c-chat-user-card .user-card .actions .bunt-button:focus, #app .c-userlist .user-list .c-chat-user-card .user-card .actions .bunt-button:focus, #app .c-exhibition .c-chat-user-card .user-card .actions .bunt-button:focus, #app .c-exhibition .exhibitor .c-chat-user-card .user-card .actions .bunt-button:focus, #app .c-exhibition .exhibitor .short-text .c-chat-user-card .user-card .actions .bunt-button:focus, #app .v-preferences .c-chat-user-card .user-card .actions .bunt-button:focus, #app .c-channel .c-chat-user-card .user-card .actions .bunt-button:focus, #app .c-chat .c-chat-user-card .user-card .actions .bunt-button:focus, #app .actions .c-chat-user-card .user-card .actions .bunt-button:focus, #app .emoji-mart .c-chat-user-card .user-card .actions .bunt-button:focus, #app .emoji-mart-category-label span .c-chat-user-card .user-card .actions .bunt-button:focus, #app .background-room .c-chat-user-card .user-card .actions .bunt-button:focus, #app .c-prompt .prompt-wrapper .c-chat-user-card .user-card .actions .bunt-button:focus, #app .c-chat-user-card .user-card .c-chat-user-card .user-card .actions .bunt-button:focus, #app .c-contact-requests .c-chat-user-card .user-card .actions .bunt-button:focus, #app .c-contact-requests .header .c-chat-user-card .user-card .actions .bunt-button:focus, #app .c-exhibitors .c-chat-user-card .user-card .actions .bunt-button:focus, #app .c-exhibitors .exhibitor-list .header .c-chat-user-card .user-card .actions .bunt-button:focus, #app .c-exhibitors .exhibitor-list .name .c-chat-user-card .user-card .actions .bunt-button:focus, #app .c-room .contact-requests-list .actions .btn-open-dm:focus, #app .c-userlist .contact-requests-list .actions .btn-open-dm:focus, #app .c-userlist .profile .content .exhibitors .exhibitor .contact-requests-list .actions .btn-open-dm:focus, #app .c-userlist .profile .content .exhibitors .exhibitor .short-text .contact-requests-list .actions .btn-open-dm:focus, #app .c-userlist .user-list .contact-requests-list .actions .btn-open-dm:focus, #app .c-exhibition .contact-requests-list .actions .btn-open-dm:focus, #app .c-exhibition .exhibitor .contact-requests-list .actions .btn-open-dm:focus, #app .c-exhibition .exhibitor .short-text .contact-requests-list .actions .btn-open-dm:focus, #app .v-preferences .contact-requests-list .actions .btn-open-dm:focus, #app .c-channel .contact-requests-list .actions .btn-open-dm:focus, #app .c-chat .contact-requests-list .actions .btn-open-dm:focus, #app .actions .contact-requests-list .actions .btn-open-dm:focus, #app .emoji-mart .contact-requests-list .actions .btn-open-dm:focus, #app .emoji-mart-category-label span .contact-requests-list .actions .btn-open-dm:focus, #app .background-room .contact-requests-list .actions .btn-open-dm:focus, #app .c-prompt .prompt-wrapper .contact-requests-list .actions .btn-open-dm:focus, #app .c-chat-user-card .user-card .contact-requests-list .actions .btn-open-dm:focus, #app .c-contact-requests .contact-requests-list .actions .btn-open-dm:focus, #app .c-contact-requests .header .contact-requests-list .actions .btn-open-dm:focus, #app .c-exhibitors .contact-requests-list .actions .btn-open-dm:focus, #app .c-exhibitors .exhibitor-list .header .contact-requests-list .actions .btn-open-dm:focus, #app .c-exhibitors .exhibitor-list .name .contact-requests-list .actions .btn-open-dm:focus {
  color: var(--link-hover) !important;
}
#app .c-room .bunt-icon-button:hover:not(.disabled), #app .c-userlist .bunt-icon-button:hover:not(.disabled), #app .c-userlist .profile .content .exhibitors .exhibitor .bunt-icon-button:hover:not(.disabled), #app .c-userlist .profile .content .exhibitors .exhibitor .short-text .bunt-icon-button:hover:not(.disabled), #app .c-userlist .user-list .bunt-icon-button:hover:not(.disabled), #app .c-exhibition .bunt-icon-button:hover:not(.disabled), #app .c-exhibition .exhibitor .bunt-icon-button:hover:not(.disabled), #app .c-exhibition .exhibitor .short-text .bunt-icon-button:hover:not(.disabled), #app .v-preferences .bunt-icon-button:hover:not(.disabled), #app .c-channel .bunt-icon-button:hover:not(.disabled), #app .c-chat .bunt-icon-button:hover:not(.disabled), #app .actions .bunt-icon-button:hover:not(.disabled), #app .emoji-mart .bunt-icon-button:hover:not(.disabled), #app .emoji-mart-category-label span .bunt-icon-button:hover:not(.disabled), #app .background-room .bunt-icon-button:hover:not(.disabled), #app .c-prompt .prompt-wrapper .bunt-icon-button:hover:not(.disabled), #app .c-chat-user-card .user-card .bunt-icon-button:hover:not(.disabled), #app .c-contact-requests .bunt-icon-button:hover:not(.disabled), #app .c-contact-requests .header .bunt-icon-button:hover:not(.disabled), #app .c-exhibitors .bunt-icon-button:hover:not(.disabled), #app .c-exhibitors .exhibitor-list .header .bunt-icon-button:hover:not(.disabled), #app .c-exhibitors .exhibitor-list .name .bunt-icon-button:hover:not(.disabled), #app .c-room .btn-emoji-picker:hover:not(.disabled), #app .c-userlist .btn-emoji-picker:hover:not(.disabled), #app .c-userlist .profile .content .exhibitors .exhibitor .btn-emoji-picker:hover:not(.disabled), #app .c-userlist .profile .content .exhibitors .exhibitor .short-text .btn-emoji-picker:hover:not(.disabled), #app .c-userlist .user-list .btn-emoji-picker:hover:not(.disabled), #app .c-exhibition .btn-emoji-picker:hover:not(.disabled), #app .c-exhibition .exhibitor .btn-emoji-picker:hover:not(.disabled), #app .c-exhibition .exhibitor .short-text .btn-emoji-picker:hover:not(.disabled), #app .v-preferences .btn-emoji-picker:hover:not(.disabled), #app .c-channel .btn-emoji-picker:hover:not(.disabled), #app .c-chat .btn-emoji-picker:hover:not(.disabled), #app .actions .btn-emoji-picker:hover:not(.disabled), #app .emoji-mart .btn-emoji-picker:hover:not(.disabled), #app .emoji-mart-category-label span .btn-emoji-picker:hover:not(.disabled), #app .background-room .btn-emoji-picker:hover:not(.disabled), #app .c-prompt .prompt-wrapper .btn-emoji-picker:hover:not(.disabled), #app .c-chat-user-card .user-card .btn-emoji-picker:hover:not(.disabled), #app .c-contact-requests .btn-emoji-picker:hover:not(.disabled), #app .c-contact-requests .header .btn-emoji-picker:hover:not(.disabled), #app .c-exhibitors .btn-emoji-picker:hover:not(.disabled), #app .c-exhibitors .exhibitor-list .header .btn-emoji-picker:hover:not(.disabled), #app .c-exhibitors .exhibitor-list .name .btn-emoji-picker:hover:not(.disabled), #app .c-room .emoji-mart-category .emoji-mart-emoji:hover::before, #app .c-userlist .emoji-mart-category .emoji-mart-emoji:hover::before, #app .c-userlist .profile .content .exhibitors .exhibitor .emoji-mart-category .emoji-mart-emoji:hover::before, #app .c-userlist .profile .content .exhibitors .exhibitor .short-text .emoji-mart-category .emoji-mart-emoji:hover::before, #app .c-userlist .user-list .emoji-mart-category .emoji-mart-emoji:hover::before, #app .c-exhibition .emoji-mart-category .emoji-mart-emoji:hover::before, #app .c-exhibition .exhibitor .emoji-mart-category .emoji-mart-emoji:hover::before, #app .c-exhibition .exhibitor .short-text .emoji-mart-category .emoji-mart-emoji:hover::before, #app .v-preferences .emoji-mart-category .emoji-mart-emoji:hover::before, #app .c-channel .emoji-mart-category .emoji-mart-emoji:hover::before, #app .c-chat .emoji-mart-category .emoji-mart-emoji:hover::before, #app .actions .emoji-mart-category .emoji-mart-emoji:hover::before, #app .emoji-mart .emoji-mart-category .emoji-mart-emoji:hover::before, #app .emoji-mart-category-label span .emoji-mart-category .emoji-mart-emoji:hover::before, #app .background-room .emoji-mart-category .emoji-mart-emoji:hover::before, #app .c-prompt .prompt-wrapper .emoji-mart-category .emoji-mart-emoji:hover::before, #app .c-chat-user-card .user-card .emoji-mart-category .emoji-mart-emoji:hover::before, #app .c-contact-requests .emoji-mart-category .emoji-mart-emoji:hover::before, #app .c-contact-requests .header .emoji-mart-category .emoji-mart-emoji:hover::before, #app .c-exhibitors .emoji-mart-category .emoji-mart-emoji:hover::before, #app .c-exhibitors .exhibitor-list .header .emoji-mart-category .emoji-mart-emoji:hover::before, #app .c-exhibitors .exhibitor-list .name .emoji-mart-category .emoji-mart-emoji:hover::before {
  background-color: var(--clr-primary-alpha-50) !important;
}
#app .c-room img[src="/media/pub/kif490/b9fde30e-0025-4432-853b-819a2fecf27e.EtEOfJLlKWym.png"], #app .c-userlist img[src="/media/pub/kif490/b9fde30e-0025-4432-853b-819a2fecf27e.EtEOfJLlKWym.png"], #app .c-userlist .profile .content .exhibitors .exhibitor img[src="/media/pub/kif490/b9fde30e-0025-4432-853b-819a2fecf27e.EtEOfJLlKWym.png"], #app .c-userlist .profile .content .exhibitors .exhibitor .short-text img[src="/media/pub/kif490/b9fde30e-0025-4432-853b-819a2fecf27e.EtEOfJLlKWym.png"], #app .c-userlist .user-list img[src="/media/pub/kif490/b9fde30e-0025-4432-853b-819a2fecf27e.EtEOfJLlKWym.png"], #app .c-exhibition img[src="/media/pub/kif490/b9fde30e-0025-4432-853b-819a2fecf27e.EtEOfJLlKWym.png"], #app .c-exhibition .exhibitor img[src="/media/pub/kif490/b9fde30e-0025-4432-853b-819a2fecf27e.EtEOfJLlKWym.png"], #app .c-exhibition .exhibitor .short-text img[src="/media/pub/kif490/b9fde30e-0025-4432-853b-819a2fecf27e.EtEOfJLlKWym.png"], #app .v-preferences img[src="/media/pub/kif490/b9fde30e-0025-4432-853b-819a2fecf27e.EtEOfJLlKWym.png"], #app .c-channel img[src="/media/pub/kif490/b9fde30e-0025-4432-853b-819a2fecf27e.EtEOfJLlKWym.png"], #app .c-chat img[src="/media/pub/kif490/b9fde30e-0025-4432-853b-819a2fecf27e.EtEOfJLlKWym.png"], #app .actions img[src="/media/pub/kif490/b9fde30e-0025-4432-853b-819a2fecf27e.EtEOfJLlKWym.png"], #app .emoji-mart img[src="/media/pub/kif490/b9fde30e-0025-4432-853b-819a2fecf27e.EtEOfJLlKWym.png"], #app .emoji-mart-category-label span img[src="/media/pub/kif490/b9fde30e-0025-4432-853b-819a2fecf27e.EtEOfJLlKWym.png"], #app .background-room img[src="/media/pub/kif490/b9fde30e-0025-4432-853b-819a2fecf27e.EtEOfJLlKWym.png"], #app .c-prompt .prompt-wrapper img[src="/media/pub/kif490/b9fde30e-0025-4432-853b-819a2fecf27e.EtEOfJLlKWym.png"], #app .c-chat-user-card .user-card img[src="/media/pub/kif490/b9fde30e-0025-4432-853b-819a2fecf27e.EtEOfJLlKWym.png"], #app .c-contact-requests img[src="/media/pub/kif490/b9fde30e-0025-4432-853b-819a2fecf27e.EtEOfJLlKWym.png"], #app .c-contact-requests .header img[src="/media/pub/kif490/b9fde30e-0025-4432-853b-819a2fecf27e.EtEOfJLlKWym.png"], #app .c-exhibitors img[src="/media/pub/kif490/b9fde30e-0025-4432-853b-819a2fecf27e.EtEOfJLlKWym.png"], #app .c-exhibitors .exhibitor-list .header img[src="/media/pub/kif490/b9fde30e-0025-4432-853b-819a2fecf27e.EtEOfJLlKWym.png"], #app .c-exhibitors .exhibitor-list .name img[src="/media/pub/kif490/b9fde30e-0025-4432-853b-819a2fecf27e.EtEOfJLlKWym.png"] {
  filter: invert(100%);
}
#app .c-room .c-avatar > img, #app .c-userlist .c-avatar > img, #app .c-userlist .profile .content .exhibitors .exhibitor .c-avatar > img, #app .c-userlist .profile .content .exhibitors .exhibitor .short-text .c-avatar > img, #app .c-userlist .user-list .c-avatar > img, #app .c-exhibition .c-avatar > img, #app .c-exhibition .exhibitor .c-avatar > img, #app .c-exhibition .exhibitor .short-text .c-avatar > img, #app .v-preferences .c-avatar > img, #app .c-channel .c-avatar > img, #app .c-chat .c-avatar > img, #app .actions .c-avatar > img, #app .emoji-mart .c-avatar > img, #app .emoji-mart-category-label span .c-avatar > img, #app .background-room .c-avatar > img, #app .c-prompt .prompt-wrapper .c-avatar > img, #app .c-chat-user-card .user-card .c-avatar > img, #app .c-contact-requests .c-avatar > img, #app .c-contact-requests .header .c-avatar > img, #app .c-exhibitors .c-avatar > img, #app .c-exhibitors .exhibitor-list .header .c-avatar > img, #app .c-exhibitors .exhibitor-list .name .c-avatar > img {
  background-color: #353535;
}
#app .c-create-dm-prompt .content p {
  color: var(--main-fg);
}
#app .c-user-search .list .list__item:hover, #app .c-user-search .list .selected, #app .c-chat-message:hover, #app .actions:not(.c-contact-requests *), #app .emoji-mart, #app .background-room, #app .c-prompt .prompt-wrapper, #app .c-user-select .search-results .user:hover, #app .c-chat-user-card .user-card, #app .c-contact-requests .contact-requests-list .tbody .table-row.active, #app .c-contact-requests .contact-requests-list .tbody .table-row:hover, #app .c-exhibitors .exhibitor-list .tbody .table-row.active, #app .c-exhibitors .exhibitor-list .tbody .table-row:hover, #app .c-exhibitors .exhibitor-list .table-row:hover .name {
  background-color: black;
}
#app input {
  background-color: black;
  border: 1px solid var(--main-fg);
  border-radius: 0;
  color: var(--main-fg);
}
#app .bunt-input .outline {
  stroke: var(--clr-sidebar-text-disabled);
}
#app .bunt-input input, #app .bunt-input label {
  color: var(--main-fg);
}
#app .bunt-input input::placeholder, #app .bunt-input label::placeholder {
  color: var(--clr-sidebar-text-disabled);
}
#app .c-chat-message .timestamp {
  color: var(--clr-sidebar-active-fg);
  font-weight: 600;
  font-size: 0.9em;
  font-family: monospaced;
}
#app .c-chat-message .message-header .timestamp {
  order: -1;
  margin-right: 1em;
  background-color: var(--clr-sidebar-active-bg);
  padding: 0 2px;
}
#app .c-chat-message .content-wrapper .preview-card {
  background-color: black;
}
#app .c-chat-message .content-wrapper .preview-card .url {
  color: white !important;
}
`;
    }

    if (document.location.host === "ak.kif.rocks") {
      styleEl.innerHTML += `
body {
  background-color: var(--main-bg);
  color: var(--main-fg);
  font-weight: 500;
  font-family: Montserrat, Roboto, Helvetica Neue, HelveticaNeue, Helvetica, Arial, Microsoft Yahei, 微软雅黑, STXihei, 华文细黑, sans-serif;
  letter-spacing: 0.02em;
}
body a {
  color: var(--clr-primary) !important;
  font-weight: 600;
  fill: currentColor !important;
}
body a:hover, body a:focus {
  color: var(--link-hover) !important;
}
body .btn:not(.disabled):hover, body .btn:not(.disabled):active {
  margin-top: 0;
  border-bottom-width: 1px;
}
body .btn-success, body .alert-success, body .badge-success {
  background: none !important;
  border: 1px solid darkgreen;
  color: darkgreen !important;
}
body .btn-success:hover, body .alert-success:hover, body .badge-success:hover, body .btn-success:focus, body .alert-success:focus, body .badge-success:focus {
  color: green;
  border-color: green;
}
body .btn-info, body .alert-info, body .badge-info {
  background: none !important;
  border: 1px solid #5151d6;
  color: #5151d6 !important;
}
body .btn-info:hover, body .alert-info:hover, body .badge-info:hover, body .btn-info:focus, body .alert-info:focus, body .badge-info:focus {
  color: #7b7bcb;
  border-color: #7b7bcb;
}
body .btn-primary, body .alert-primary, body .badge-primary {
  background: none !important;
  border: 1px solid var(--clr-primary);
  color: var(--clr-primary) !important;
}
body .btn-primary:hover, body .alert-primary:hover, body .badge-primary:hover, body .btn-primary:focus, body .alert-primary:focus, body .badge-primary:focus {
  color: var(--clr-primary-darken-20);
  border-color: var(--clr-primary-darken-20);
}
body .btn-light, body .alert-light, body .badge-light, body .btn-dark, body .alert-dark, body .badge-dark {
  background: none !important;
  border: 1px solid var(--clr-sidebar-text-secondary);
  color: var(--clr-sidebar-text-secondary) !important;
}
body .btn-light:hover, body .alert-light:hover, body .badge-light:hover, body .btn-dark:hover, body .alert-dark:hover, body .badge-dark:hover, body .btn-light:focus, body .alert-light:focus, body .badge-light:focus, body .btn-dark:focus, body .alert-dark:focus, body .badge-dark:focus {
  color: var(--clr-sidebar-text-primary);
  border-color: var(--clr-sidebar-text-primary);
}
body .btn-warning, body .alert-warning, body .badge-warning {
  background: none !important;
  border: 1px solid #ae6323;
  color: #ae6323 !important;
}
body .btn-warning:hover, body .alert-warning:hover, body .badge-warning:hover, body .btn-warning:focus, body .alert-warning:focus, body .badge-warning:focus {
  color: #c88e5e;
  border-color: #c88e5e;
}
body .btn-danger, body .alert-danger, body .badge-danger {
  background: none !important;
  border: 1px solid #b84343;
  color: #b84343 !important;
}
body .btn-danger:hover, body .alert-danger:hover, body .badge-danger:hover, body .btn-danger:focus, body .alert-danger:focus, body .badge-danger:focus {
  color: #db7474;
  border-color: #db7474;
}
body .breadcrumb, body .nav-tabs .nav-link.active, body .nav-tabs .nav-item.show .nav-link, body .table, body .table thead th, body .table th, body .table td, body .dropdown-menu, body .nav-pills .nav-link.active, body .nav-pills .show > .nav-link, body .jumbotron {
  background-color: black;
  border-color: var(--clr-text-secondary);
  color: var(--main-fg);
}
body .dropdown-item:hover, body .dropdown-item:focus {
  background-color: var(--clr-sidebar-active-bg);
}
body .breadcrumb {
  border: none;
}
body .nav-tabs .nav-link, body .nav-tabs .nav-link.disabled, body .nav-tabs .nav-link.disabled:hover, body .nav-tabs .nav-link.disabled:focus, body .nav-tabs {
  border-color: var(--clr-text-secondary) !important;
}
body .table-bordered, body .table-bordered th, body .table-bordered td {
  border-color: var(--clr-text-disabled) !important;
}
body .nav-tabs .nav-link:hover, body .nav-tabs .nav-link:focus {
  border-color: var(--clr-primary) !important;
}
body .breadcrumb-item.active {
  color: var(--clr-sidebar-text-disabled);
}
body .text-dark {
  color: var(--clr-sidebar-hover-fg) !important;
}
body a.text-dark {
  color: var(--clr-primary) !important;
}
body a.text-dark:hover, body a.text-dark:focus {
  color: var(--link-hover) !important;
}
body small, body .small {
  font-size: 0.85em;
  letter-spacing: 0.1em;
}
`;
    }
  
  if (document.location.host === "wiki.kif.rocks") {
    styleEl.innerHTML += `
#column-one {
  display: none;
}
.mw-body {
  margin: 0;
  border: none;
}
.mw-body, .mw-footer, body, .toc, .mw-warning, .toccolours {
  background-color: var(--main-bg);
  color: var(--main-fg);
  font-weight: 500;
  font-family: Montserrat, Roboto, Helvetica Neue, HelveticaNeue, Helvetica, Arial, Microsoft Yahei, 微软雅黑, STXihei, 华文细黑, sans-serif;
  letter-spacing: 0.02em;
}
.mw-body a, .mw-footer a, body a, .toc a, .mw-warning a, .toccolours a {
  color: var(--clr-primary) !important;
  font-weight: 600;
  fill: currentColor !important;
}
.mw-body a:hover, .mw-footer a:hover, body a:hover, .toc a:hover, .mw-warning a:hover, .toccolours a:hover, .mw-body a:focus, .mw-footer a:focus, body a:focus, .toc a:focus, .mw-warning a:focus, .toccolours a:focus {
  color: var(--link-hover) !important;
}
.toc, .mw-warning, .toccolours, code {
  background-color: black;
  color: var(--main-fg);
  border: none;
}
.tocnumber {
  color: var(--main-fg);
}
h1, h2, h3, h4, h5, h6 {
  color: #c1c0c0;
}
img {
  background-color: #353535;
}
`;
  }
  
  if (document.location.host.endsWith(".meet.gwdg.de")) {
    styleEl.innerHTML += `
html {
  min-height: 100%;
  background: black;
}
body, .ReactModal__Content, .audioBtn--1H6rCK span:last-child, .chat--111wNM, .note--1ESx6q, .listItem--Siv4F, .hideBtn--2kqDJM, #outerdocbody, .toolbar ul li a, .chatListItem--Z2sTdzU {
  background-color: var(--main-bg) !important;
  color: var(--main-fg);
  font-weight: 500;
  font-family: Montserrat, Roboto, Helvetica Neue, HelveticaNeue, Helvetica, Arial, Microsoft Yahei, 微软雅黑, STXihei, 华文细黑, sans-serif;
  letter-spacing: 0.02em;
}
body a, .ReactModal__Content a, .audioBtn--1H6rCK span:last-child a, .chat--111wNM a, .note--1ESx6q a, .listItem--Siv4F a, .hideBtn--2kqDJM a, #outerdocbody a, .toolbar ul li a a, .chatListItem--Z2sTdzU a, body button, .ReactModal__Content button, .audioBtn--1H6rCK span:last-child button, .chat--111wNM button, .note--1ESx6q button, .listItem--Siv4F button, .hideBtn--2kqDJM button, #outerdocbody button, .toolbar ul li a button, .chatListItem--Z2sTdzU button, body .hideBtn--ZOkOfz, .ReactModal__Content .hideBtn--ZOkOfz, .audioBtn--1H6rCK span:last-child .hideBtn--ZOkOfz, .chat--111wNM .hideBtn--ZOkOfz, .note--1ESx6q .hideBtn--ZOkOfz, .listItem--Siv4F .hideBtn--ZOkOfz, .hideBtn--2kqDJM .hideBtn--ZOkOfz, #outerdocbody .hideBtn--ZOkOfz, .toolbar ul li a .hideBtn--ZOkOfz, .chatListItem--Z2sTdzU .hideBtn--ZOkOfz {
  color: var(--clr-primary) !important;
  font-weight: 600;
  fill: currentColor !important;
}
body a:hover, .ReactModal__Content a:hover, .audioBtn--1H6rCK span:last-child a:hover, .chat--111wNM a:hover, .note--1ESx6q a:hover, .listItem--Siv4F a:hover, .hideBtn--2kqDJM a:hover, #outerdocbody a:hover, .toolbar ul li a a:hover, .chatListItem--Z2sTdzU a:hover, body button:hover, .ReactModal__Content button:hover, .audioBtn--1H6rCK span:last-child button:hover, .chat--111wNM button:hover, .note--1ESx6q button:hover, .listItem--Siv4F button:hover, .hideBtn--2kqDJM button:hover, #outerdocbody button:hover, .toolbar ul li a button:hover, .chatListItem--Z2sTdzU button:hover, body .hideBtn--ZOkOfz:hover, .ReactModal__Content .hideBtn--ZOkOfz:hover, .audioBtn--1H6rCK span:last-child .hideBtn--ZOkOfz:hover, .chat--111wNM .hideBtn--ZOkOfz:hover, .note--1ESx6q .hideBtn--ZOkOfz:hover, .listItem--Siv4F .hideBtn--ZOkOfz:hover, .hideBtn--2kqDJM .hideBtn--ZOkOfz:hover, #outerdocbody .hideBtn--ZOkOfz:hover, .toolbar ul li a .hideBtn--ZOkOfz:hover, .chatListItem--Z2sTdzU .hideBtn--ZOkOfz:hover, body a:focus, .ReactModal__Content a:focus, .audioBtn--1H6rCK span:last-child a:focus, .chat--111wNM a:focus, .note--1ESx6q a:focus, .listItem--Siv4F a:focus, .hideBtn--2kqDJM a:focus, #outerdocbody a:focus, .toolbar ul li a a:focus, .chatListItem--Z2sTdzU a:focus, body button:focus, .ReactModal__Content button:focus, .audioBtn--1H6rCK span:last-child button:focus, .chat--111wNM button:focus, .note--1ESx6q button:focus, .listItem--Siv4F button:focus, .hideBtn--2kqDJM button:focus, #outerdocbody button:focus, .toolbar ul li a button:focus, .chatListItem--Z2sTdzU button:focus, body .hideBtn--ZOkOfz:focus, .ReactModal__Content .hideBtn--ZOkOfz:focus, .audioBtn--1H6rCK span:last-child .hideBtn--ZOkOfz:focus, .chat--111wNM .hideBtn--ZOkOfz:focus, .note--1ESx6q .hideBtn--ZOkOfz:focus, .listItem--Siv4F .hideBtn--ZOkOfz:focus, .hideBtn--2kqDJM .hideBtn--ZOkOfz:focus, #outerdocbody .hideBtn--ZOkOfz:focus, .toolbar ul li a .hideBtn--ZOkOfz:focus, .chatListItem--Z2sTdzU .hideBtn--ZOkOfz:focus {
  color: var(--link-hover) !important;
}
.toolbar ul li a.selected {
  background-color: var(--clr-bbb-background);
}
.chatNameMain--Z1S9hh4, .userNameMain--2fo2zM, .hideBtn--ZOkOfz > i {
  color: var(--main-fg);
}
.toc, .mw-warning, .toccolours, code, .systemMessage--ZYspJQ, .chatListItem--Z2sTdzU:hover, .active--Z1SuO2X, .userListItem--Z1qtuLG:hover, .userListItem--Z1qtuLG:focus, .listItem--Siv4F:hover, #innerdocbody, .hideBtn--2kqDJM:hover, .toolbar, .hideBtn--ZOkOfz, .toolbar ul li a:hover, .hideBtn--ZOkOfz:hover, .usertListItemWithMenu--Z27EKt2 {
  background-color: black;
  color: var(--main-fg);
  border: none;
  box-shadow: none;
}
.input--2wilPX {
  background-color: black;
  color: var(--main-fg);
  border: 1px solid var(--main-fg);
  box-shadow: none;
}
.input--2wilPX:focus {
  border-color: var(--clr-primary);
}
.audioBtn--1H6rCK span:first-child, .audioBtn--1H6rCK:hover span:first-child, .audioBtn--1H6rCK:focus span:first-child {
  background-color: black;
  border: 5px solid var(--clr-text-disabled);
}
.audioBtn--1H6rCK:hover span:first-child, .audioBtn--1H6rCK:focus span:first-child {
  border-color: var(--clr-primary);
}
.audioBtn--1H6rCK i {
  color: var(--clr-text-disabled);
}
.messageList--hsNac {
  background: none;
}
html, .scrollableList--Z2s6Her, .messageList--hsNac {
  scrollbar-color: rgba(255, 255, 255, 0.31) rgba(244, 241, 241, 0.1);
  scrollbar-width: thin;
}
.userlistPad--o5KDX {
  background-color: rgba(149, 226, 230, 0.27);
}
`;
  }
})();
