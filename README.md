# KIF 49,0 dark mode

## Installation

Zum Installieren benoetigst du ein browser plugin fuer user scripte. In Firefox empfehle ich [ViolentMonkey](https://addons.mozilla.org/en-US/firefox/addon/violentmonkey) (TamperMonkey fuehrt zu Problemen ein paar Seiten im Konferenzsystem).

Einfach das [script.user.js](https://gitlab.com/xoria/kif-49.0-dark-theme/-/raw/master/script.user.js) in dem Plugin hinzufuegen und [event.kif.rocks](https://event.kif.rocks/) neu laden.
